package com.shankar.datatoggle;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link DataToggleWidgetConfigureActivity DataToggleWidgetConfigureActivity}
 */
public class DataToggleWidget extends AppWidgetProvider {
    private static final String MyOnClick = "myOnClickTag";

    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        // if we brodcast than definetly we have to define ONRECEIVE
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (MyOnClick.equals(intent.getAction())) {
            Log.w("Click", "Toggle  recieve activated");
            toggleData(context);

        }

    }

    private void toggleData(Context context) {


        if (isOnline(context)) {
            if (goOffline()) {
                Toasty.error(context, context.getString(R.string.message_data_off), Toast.LENGTH_SHORT, true).show();
            }
        } else {
            if(goOnline()){
                Toasty.success(context, context.getString(R.string.message_data_on), Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        CharSequence widgetText = DataToggleWidgetConfigureActivity.loadTitlePref(context, appWidgetId);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.data_toggle_widget);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
//        this.context = context;
        Log.w("Click", "Toggle activated");
        Log.w("Click", "Toggle says hi");
        for (int widgetId : appWidgetIds) {
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.data_toggle_widget);
            views.setOnClickPendingIntent(R.id.data_toggle, getPendingSelfIntent(context, MyOnClick));
            appWidgetManager.updateAppWidget(widgetId, views);

        }

    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // When the user deletes the widget, delete the preference associated with it.
        for (int appWidgetId : appWidgetIds) {
            DataToggleWidgetConfigureActivity.deleteTitlePref(context, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    private boolean goOnline() {
        //TODO:Turn on net and 4g
       /* if (isOnline()) {
            return true;
        } else {
            return false;
        }*/
        return true;
    }

    private boolean goOffline() {
        //TODO:Turn off net and 4g
      /*  if (!isOnline()) {
            return true;
        } else {
            return false;
        }*/
        return true;
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    public boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}

